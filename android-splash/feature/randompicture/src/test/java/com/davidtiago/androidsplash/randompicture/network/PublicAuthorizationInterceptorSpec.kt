package com.davidtiago.androidsplash.randompicture.network

import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import kotlin.test.assertEquals

const val SAMPLE_API_KEY = "SAMPLE_API_KEY"

object PublicAuthorizationInterceptorSpec : Spek({
    val interceptor by memoized { PublicAuthorizationInterceptor(SAMPLE_API_KEY) }
    val server by memoized { MockWebServer() }
    describe("Adds authorization header") {
        beforeEachTest {
            server.enqueue(MockResponse())
            val okHttpClient = OkHttpClient().newBuilder()
                .addInterceptor(interceptor)
                .build()
            okHttpClient.newCall(
                Request.Builder()
                    .url(
                        server.url("/")
                    )
                    .build()
            ).execute()
        }
        afterEachTest { server.shutdown() }
        it("should add interceptor") {
            val recordedRequest = server.takeRequest()
            assertEquals(
                expected = "Client-ID $SAMPLE_API_KEY",
                actual = recordedRequest.getHeader("Authorization")
            )
        }
    }
})
