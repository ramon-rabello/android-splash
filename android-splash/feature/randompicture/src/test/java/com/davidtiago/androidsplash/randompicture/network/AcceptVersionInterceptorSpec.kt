package com.davidtiago.androidsplash.randompicture.network

import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.spekframework.spek2.Spek
import org.spekframework.spek2.style.specification.describe
import kotlin.test.assertEquals

object AcceptVersionInterceptorSpec : Spek({
    val interceptor by memoized { AcceptVersionInterceptor() }
    val server by memoized { MockWebServer() }
    describe("Adds version header") {
        beforeEachTest {
            server.enqueue(MockResponse())
            val okHttpClient = OkHttpClient().newBuilder()
                .addInterceptor(interceptor)
                .build()
            okHttpClient.newCall(
                Request.Builder()
                    .url(
                        server.url("/")
                    )
                    .build()
            ).execute()
        }
        afterEachTest { server.shutdown() }
        it("should add interceptor") {
            val recordedRequest = server.takeRequest()
            assertEquals(
                expected = "v1",
                actual = recordedRequest.getHeader("Accept-Version")
            )
        }
    }
})
