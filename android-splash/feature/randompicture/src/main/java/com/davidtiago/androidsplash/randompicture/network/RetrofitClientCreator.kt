package com.davidtiago.androidsplash.randompicture.network

import com.davidtiago.androidsplash.randompicture.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Named

class RetrofitClientCreator @Inject constructor(
    @Named("baseUrl") private val baseUrl: String,
    private val acceptVersionInterceptor: AcceptVersionInterceptor,
    private val publicAuthorizationInterceptor: PublicAuthorizationInterceptor
) {
    fun create(): Retrofit = Retrofit.Builder()
        .baseUrl(baseUrl)
        .client(buildOkHttp())
        .build()

    private fun buildOkHttp(): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptors()
            .build()

    private fun OkHttpClient.Builder.addInterceptors(): OkHttpClient.Builder {
        addInterceptor(publicAuthorizationInterceptor)
        addInterceptor(acceptVersionInterceptor)
        addLogInterceptor()
        return this
    }

    private fun OkHttpClient.Builder.addLogInterceptor() {
        if (BuildConfig.DEBUG) {
            addInterceptor(
                HttpLoggingInterceptor().apply {
                    level = HttpLoggingInterceptor.Level.BODY
                })
        }
    }
}
