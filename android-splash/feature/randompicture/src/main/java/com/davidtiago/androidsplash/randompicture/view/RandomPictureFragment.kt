package com.davidtiago.androidsplash.randompicture.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.observe
import com.davidtiago.androidsplash.randompicture.R
import com.davidtiago.androidsplash.randompicture.databinding.RandomPictureFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RandomPictureFragment : Fragment() {

    private val viewModel: RandomPictureViewModel by viewModels()

    private var _binding: RandomPictureFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = RandomPictureFragmentBinding.inflate(inflater, container, false)
        viewModel.viewState.observe(viewLifecycleOwner) { state ->
            when (state) {
                is RandomPictureViewState.Loaded -> binding.pictureDescription.text =
                    state.randomPicture.description
                RandomPictureViewState.Loading ->
                    binding.pictureDescription.setText(R.string.loading)
                RandomPictureViewState.LoadError ->
                    binding.pictureDescription.setText(R.string.error)
            }
        }
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
