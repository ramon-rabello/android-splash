package com.davidtiago.androidsplash.randompicture.data

data class RandomPicture(
    val description: String
)
