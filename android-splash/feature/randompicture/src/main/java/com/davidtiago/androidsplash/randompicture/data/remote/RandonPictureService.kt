package com.davidtiago.androidsplash.randompicture.data.remote

import com.davidtiago.androidsplash.randompicture.data.RandomPicture
import retrofit2.http.GET

interface RandonPictureService {
    @GET("/photos/random")
    suspend fun get(): RandomPicture
}
